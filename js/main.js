let dataGlasses = [
    { id: "G1", src: "./img/g1.jpg", virtualImg: "./img/v1.png", brand: "Armani Exchange", name: "Bamboo wood", color: "Brown", price: 150, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? " },
    { id: "G2", src: "./img/g2.jpg", virtualImg: "./img/v2.png", brand: "Arnette", name: "American flag", color: "American flag", price: 150, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G3", src: "./img/g3.jpg", virtualImg: "./img/v3.png", brand: "Burberry", name: "Belt of Hippolyte", color: "Blue", price: 100, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G4", src: "./img/g4.jpg", virtualImg: "./img/v4.png", brand: "Coarch", name: "Cretan Bull", color: "Red", price: 100, description: "In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G5", src: "./img/g5.jpg", virtualImg: "./img/v5.png", brand: "D&G", name: "JOYRIDE", color: "Gold", price: 180, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?" },
    { id: "G6", src: "./img/g6.jpg", virtualImg: "./img/v6.png", brand: "Polo", name: "NATTY ICE", color: "Blue, White", price: 120, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G7", src: "./img/g7.jpg", virtualImg: "./img/v7.png", brand: "Ralph", name: "TORTOISE", color: "Black, Yellow", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam." },
    { id: "G8", src: "./img/g8.jpg", virtualImg: "./img/v8.png", brand: "Polo", name: "NATTY ICE", color: "Red, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim." },
    { id: "G9", src: "./img/g9.jpg", virtualImg: "./img/v9.png", brand: "Coarch", name: "MIDNIGHT VIXEN REMIX", color: "Blue, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet." }
];
function getEl(id) {
    return document.getElementById(id);
};
var gl=getEl("vglassesList");
var glInfo=getEl("glassesInfo");
var avatar=getEl("avatar");
gl.innerHTML="";

export function getGlassInfo(id){
    glInfo.innerHTML=
    
    `
    <h4>${dataGlasses[id].name + " - " +dataGlasses[id].brand}</h4>
    <a id="price" class="btn btn-danger" role="button">${dataGlasses[id].price}</a>
    <p>${dataGlasses[id].description}</p>
    `;
    glInfo.style.display='block';
    avatar.innerHTML=`
    <img src="${dataGlasses[id].virtualImg}" alt="" srcset="" />
    `
    return  ;
    };

let listGlass=(()=>{
    
for (let i = 0; i < dataGlasses.length; i++) {
   var a= `<img src="${dataGlasses[i].virtualImg}" alt="" srcset="" class="col-4 my-3" onclick="getGlassInfo(${i})" >`;
   
    gl.innerHTML+=a;
    
    // if (document.getElementById(i).onclick==true) {
    //     getGlassInfo(i)
    //  }
// if (document.getElementById(i).click==true) {
//     getEl(i).addEventListener("click",getGlassInfo(i));
// }
    
}

});
listGlass();
window.getGlassInfo = getGlassInfo

// for (let i = 0; i < dataGlasses.length; i++) {
//     getEl(i).onclick=getGlassInfo(i);
 
// }

//  <input   id="${i}" class="col-4 my-3" type="image" src="${dataGlasses[i].virtualImg}" />


